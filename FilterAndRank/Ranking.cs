using System.Collections.Generic;
using System.Linq;

namespace FilterAndRank
{
    public static class Ranking
    {        
        public static IList<RankedResult> FilterByCountryWithRank(
            IList<Person> people,
            IList<CountryRanking> rankingData,
            IList<string> countryFilter,
            int minRank, int maxRank,
            int maxCount)
        {
            // TODO: write your solution here.  Do not change the method signature in any way, or validation of your solution will fail.

            var result= people.Join(rankingData,
                      p => p.Id,
                      r => r.PersonId,
                      (p, r) => new {p.Id, p.Name, r.Country, r.Rank })
                 .Where(o => countryFilter.Any(x => x.ToLower() == o.Country.ToLower())
                 && o.Rank >=minRank 
                 && o.Rank<=maxRank)
                 .OrderBy(o=>o.Rank)
                 .ThenBy(o=>o.Country.ToLower())
                 .ThenBy(o=>o.Name.ToLower())
                .Select(p => new RankedResult(p.Id, p.Rank))
                .AsEnumerable();


            
            return result.TakeWhile(o=> o.Rank <= result.ElementAt(maxCount-1).Rank).ToList();
        }
    }
}
